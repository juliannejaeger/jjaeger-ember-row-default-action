'use strict';

module.exports = {
  extends: 'recommended',
  rules: {
    'attribute-indentation': false,
    'block-indentation': false,
    'img-alt-attributes': false,
    'linebreak-style': false,
    'link-rel-noopener': false,
    'no-attrs-in-components': false,
    'no-html-comments': false,
    'no-inline-styles': false,
    'no-input-block': false,
    'no-input-tagname': false,
    'no-invalid-interactive': false,
    'no-unnecessary-concat': false,
    'no-triple-curlies': false,
    'quotes': false,
    'self-closing-void-elements': false,
    'simple-unless': false,
    'table-groups': false
  }
};
