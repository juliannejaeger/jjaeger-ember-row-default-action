import Component from '@ember/component';

export default Component.extend({
  tagName: 'tr',
  classNames: null,
  disabledElements: null,
  disabled: false,
  init() {
    this._super(...arguments);
    this.set('classNames', ['pointer']);
    this.set('disabledElements', ['i', 'a']);
  },
  currentRowHovered(event) {
    let classArray = event.target.className.split(' ');
    let disabledElements = this.get('disabledElements');
    let disabled = this.get('disabled');

    //when move mouse on the row, if the current object scrolling over has the
    //class of disable-default-action, set current-row to null
    //'i' and 'a' are excluded to make sure you can't click context menu items.
    if(classArray.indexOf("disable-default-action") > -1 || disabledElements.indexOf(event.target.localName) > -1 || disabled){
      return false;
    }
    return true;
  },
  mouseLeave(){
    this.set('current-row', null);
  },
  mouseMove(event){
    if (!this.currentRowHovered(event)) {
      this.set('current-row', null);
    }else{
      this.set('current-row', this.get('row-id'));
    }
  },
  click(event){
    if (!this.currentRowHovered(event)) {
      return;
    }

    this.set('current-row', this.get('row-id'));
    this.sendAction();
  }
});
