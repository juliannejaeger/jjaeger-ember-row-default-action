import EmberObject from '@ember/object';
import Controller from '@ember/controller';
import { A } from '@ember/array';
import faker from 'faker';

export default Controller.extend({
  options: null,
  conflicts: null,
  init() {
    this._super(...arguments);

    var object = EmberObject.extend({});
    var items = A([]);

    for (let i = 0; i < 10; i++) {
      var row = object.create();

      row.set("id", faker.random.number());
      row.set("name", faker.company.companyName());
      row.set("email", faker.internet.email());

      items.push(row);
    }

    this.set('items', items);
  },

  actions: {
    defaultAction(){
      this.transitionToRoute('items.edit', this.get('current-row'));
    }
  }
});
