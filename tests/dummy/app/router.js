import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('items', function() {
    this.route('edit', {path: 'edit/:item_id'});
  });
});

export default Router;
