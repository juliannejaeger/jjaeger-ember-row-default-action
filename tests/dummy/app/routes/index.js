import Route from '@ember/routing/route';

export default Route.extend({
  excludedRoutes: null,

  init() {
    this._super(...arguments);
  },

  redirect: function(model, transition) {
    this._super(model, transition);

  }
});
